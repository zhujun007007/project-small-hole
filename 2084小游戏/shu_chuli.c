#include"shu_chuli.h"
#include<stdio.h>
#include<stdlib.h>
#include "bmp.h"
#include "lcd.h"

void sheng_suijishu( int (*a)[4],int i0 ,int j0)
{
    int i = rand()%(i0);
    int j = rand()%(j0);
    printf("行号%d\n",i);
    printf("列号%d\n",j);
    for(i;i<i0;i++)
    {
        for(j;j<j0;j++)
        {
            if(a[i][j]==0)
            {
                while(1)
                { 
                    a[i][j] = rand()%3;
                    printf("1新的随机数%d\n",a[i][j]);
                    if(a[i][j] ==2 )
                    {
                        printf("行号%d\n",i);
                        printf("列号%d\n",j);
                        return ;
                    }   
                    a[i][j] = rand()%5;
                    printf("2新的随机数%d\n",a[i][j]);
                    if(a[i][j] ==2 || a[i][j] ==4)
                    {
                        printf("行号%d\n",i);
                        printf("列号%d\n",j);
                        return ;
                    }                   
                } 
            }
        }
    }

    for(i=0;i<i0;i++)
    {
        for(j=0;j<j0;j++)
        {
            if(a[i][j]==0)
            {
                while(1)
                { 
                    a[i][j] = rand()%3;
                    printf("3新的随机数%d\n",a[i][j]);
                    if(a[i][j] ==2 )
                    {
                        printf("行号%d\n",i);
                        printf("列号%d\n",j);
                        return ;
                    }   
                    a[i][j] = rand()%5;
                    printf("4新的随机数%d\n",a[i][j]);
                    if(a[i][j] ==2 || a[i][j] ==4)
                    {
                        printf("行号%d\n",i);
                        printf("列号%d\n",j);
                        return ;
                    }                   
                } 
            }
        }
    }
    return;
}

int shu_zuo_zuohe( int (*a)[4],int i0 ,int j0)//左滑处理
{
    int return_youxiaocz = 0;
    int i,j;
    int k;
    int cao_zuo_shu=0;
    for(i=0;i<i0;i++)
    {
        for(j=0;j<j0;j++)
        {
            if(a[i][j]==0)
            {
                continue;
            }
            for(k=j+1;k<j0;k++)
            {
                if(a[i][k] == 0)
                {
                    continue;
                }
                if(a[i][k] == a[i][j])
                {
                    a[i][j] = a[i][j]+a[i][k];
                    a[i][k] = 0;
                    cao_zuo_shu = 1;
                    return_youxiaocz = 1;
                    break;
                }
                else
                {
                    break;
                }
            }
        }
        for(j=0,k=0;j<j0;j++)//数字移动到左边
        {
           if(a[i][j] == 0)
           {
               continue;
           }
           else if(k == j)
           {
               k++;
           }
           else if(k != j)
           {
               a[i][k] = a[i][j];
               k++;//防止段错误
               cao_zuo_shu = 1;
               return_youxiaocz = 1;
           }
        }
        while(k!=0&&k<j0)
        {
            a[i][k] = 0;//可能有问题
            k++;
        }
    }
    for(i = 0;i<4;i++)
    {
        for(j=0;j<4;j++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
    printf("-----排序之后\n");

    return cao_zuo_shu;
}


int shu_you_youhe( int (*a)[4],int i0 ,int j0)//右滑处理
{
    int return_youxiaocz = 0;
    int i,j;
    int k;
    int cao_zuo_shu=0;
    for(i=0;i<i0;i++)
    {
        for(j=j0-1;j>=0;j--)
        {
            if(a[i][j]==0)
            {
                continue;
            }
            for(k=j-1;k>=0;k--)
            {
                if(a[i][k] == 0)
                {
                    continue;
                }
                if(a[i][k] == a[i][j])
                {
                    a[i][j] = a[i][j]+a[i][k];
                    a[i][k] = 0;
                    cao_zuo_shu = 1;
                    return_youxiaocz = 1;
                    break;
                }
                else
                {
                    break;
                }
            }
        }
        for(j=j0-1,k=j0-1;j>=0;j--)//数字移动到右边
        {
           if(a[i][j] == 0)
           {
               continue;
           }
           else if(k == j)
           {
               k--;
           }
           else if(k != j)
           {
               a[i][k] = a[i][j];
               k--;//防止段错误
               cao_zuo_shu = 1;
               return_youxiaocz = 1;
           }
        }
        while(k!=j0-1 && k>-1)
        {
            a[i][k] = 0;
            k--;
        }
    }
    for(i = 0;i<4;i++)
    {
        for(j=0;j<4;j++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
    printf("-----排序之后\n");

    return cao_zuo_shu;
}




int shu_shang_shanghe( int (*a)[4],int i0 ,int j0)//上滑处理
{
    int return_youxiaocz = 0;
    int i,j;
    int k;
    int cao_zuo_shu=0;
    for(j=0;j<j0;j++)
    {
        for(i=0;i<i0;i++)
        {
        
            if(a[i][j]==0)
            {
                continue;
            }
            for(k=i+1;k<i0;k++)
            {
                if(a[k][j] == 0)
                {
                    continue;
                }
                if(a[k][j] == a[i][j])
                {
                    a[i][j] = a[i][j]+a[k][j];
                    a[k][j] = 0;
                    cao_zuo_shu = 1;
                    return_youxiaocz = 1;
                    break;
                }
                else
                {
                    break;
                }
            }
        }
        for(i=0,k=0;i<i0;i++)//数字移动到上边
        {
           if(a[i][j] == 0)
           {
               continue;
           }
           else if(k == i)
           {
               k++;
           }
           else if(k != i)
           {
               a[k][j] = a[i][j];
               k++;//防止段错误
               cao_zuo_shu = 1;
               return_youxiaocz = 1;
           }
        }
        while(k!=0&&k<i0)
        {
            a[k][j] = 0;//可能有问题
            k++;
        }
    }
    for(i = 0;i<4;i++)
    {
        for(j=0;j<4;j++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
    printf("-----排序之后\n");

    return cao_zuo_shu;
}

int shu_xia_xiahe( int (*a)[4],int i0 ,int j0)//下滑处理
{
    int return_youxiaocz = 0;
    int i,j;
    int k;
    int cao_zuo_shu=0;
    for(j=0;j<j0;j++)
    {
        for(i=i0-1;i>=0;i--)
        {
        
            if(a[i][j]==0)
            {
                continue;
            }
            for(k=i-1;k>=0;k--)
            {
                if(a[k][j] == 0)
                {
                    continue;
                }
                if(a[k][j] == a[i][j])
                {
                    a[i][j] = a[i][j]+a[k][j];
                    a[k][j] = 0;
                    cao_zuo_shu = 1;
                    return_youxiaocz = 1;
                    break;
                }
                else
                {
                    break;
                }
            }
        }
        for(i=i0-1,k=i0-1;i>=0;i--)//数字移动到上边
        {
           if(a[i][j] == 0)
           {
               continue;
           }
           else if(k == i)
           {
               k--;
           }
           else if(k != i)
           {
               a[k][j] = a[i][j];
               k--;//防止段错误
               cao_zuo_shu = 1;
               return_youxiaocz = 1;
           }
        }
        while(k!=i0-1&&k>-1)
        {
            a[k][j] = 0;//可能有问题
            k--;
        }
    }
    for(i = 0;i<4;i++)
    {
        for(j=0;j<4;j++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
    printf("-----排序之后\n");

    return cao_zuo_shu;
}



void hua_tu_shu(lcd * s,int (*a)[4],int i0 ,int j0)
{
    int i,j;
    int b[4] = {0,199,399,599};
    int c[4] = {0,119,239,359};
    for(i = 0;i<i0;i++)
    {
        for(j=0;j<j0;j++)
        {
            if(a[i][j] == 0)
            {
                bmp_display(s,b[j],c[i],"./0.bmp");
            }
            else if(a[i][j] == 2)
            {
                bmp_display(s,b[j],c[i],"./2.bmp");
            }
            else if(a[i][j] == 4)
            {
                bmp_display(s,b[j],c[i],"./4.bmp");
            }
            else if(a[i][j] == 8)
            {
                bmp_display(s,b[j],c[i],"./8.bmp");
            }
            else if(a[i][j] == 16)
            {
                bmp_display(s,b[j],c[i],"./16.bmp");
            }
            else if(a[i][j] == 32)
            {
                bmp_display(s,b[j],c[i],"./32.bmp");
            }
            else if(a[i][j] == 64)
            {
                bmp_display(s,b[j],c[i],"./64.bmp");
            }
            else if(a[i][j] == 128)
            {
                bmp_display(s,b[j],c[i],"./128.bmp");
            }
            else if(a[i][j] == 256)
            {
                bmp_display(s,b[j],c[i],"./256.bmp");
            }
            else if(a[i][j] == 512)
            {
                bmp_display(s,b[j],c[i],"./512.bmp");
            }
            else if(a[i][j] == 1024)
            {
                bmp_display(s,b[j],c[i],"./1024.bmp");
            }
            else if(a[i][j] == 2048)
            {
                bmp_display(s,b[j],c[i],"./2048.bmp");
            }
        }
    }
    return;
}

int jie_shu(int (*a)[4],int i0 ,int j0)
{
    int i,j;
    for(i=0;i<i0;i++)
    {
        for(j=0;j<j0;j++)
        {
            if(a[i][j] == 2048)
            {
                return 1;
            }
            if(a[i][j] == 0)
            {
                return 2;
            }
        }
    }
    for(i=0;i<i0;i++)
    {
        for(j=0;j<j0;j++)
        {
            if(i == 0)
            {
                if(j == 0)
                {
                    if(a[i][j] == a[i+1][j] || a[i][j] == a[i][j+1])
                    {
                        return 3;
                    }
                }
                else if(j == j0-1)
                {
                    if(a[i][j] == a[i+1][j] || a[i][j] == a[i][j-1])
                    {
                        return 4;
                    }
                }
                else
                {
                    if(a[i][j] == a[i+1][j] || a[i][j] == a[i][j+1] || a[i][j] == a[i][j-1])
                    {
                        return 5;
                    }
                }
            }
            if(i == i0-1)
            {
                if(j == 0)
                {
                    if(a[i][j] == a[i-1][j] || a[i][j] == a[i][j+1])
                    {
                        return 6;
                    }
                }
                else if(j == j0-1)
                {
                    if(a[i][j] == a[i-1][j] || a[i][j] == a[i][j-1])
                    {
                        return 7;
                    }
                }
                else
                {
                    if(a[i][j] == a[i-1][j] || a[i][j] == a[i][j-1] || a[i][j] == a[i][j+1])
                    {
                        return 8;
                    }
                }
            }
            if(j == 0)
            {
                if(i == 0 )
                {
                    if(a[i][j] == a[i+1][j] || a[i][j] == a[i][j+1])
                    {
                        return 9;
                    }
                }
                else if(i == i0-1)
                {
                    if(a[i][j] == a[i-1][j] || a[i][j] == a[i][j+1])
                    {
                        return 10;
                    }
                }
                else
                {
                    if(a[i][j] == a[i+1][j] || a[i][j] == a[i-1][j] || a[i][j] == a[i][j+1])
                    {
                        return 11;
                    }
                }
            }
            if(j == j0 -1)
            {
                if(i == 0 )
                {
                    if(a[i][j] == a[i+1][j] || a[i][j] == a[i][j-1])
                    {
                        return 12;
                    }
                }
                else if(i == i0-1)
                {
                    if(a[i][j] == a[i-1][j] || a[i][j] == a[i][j-1])
                    {
                        return 13;
                    }
                }
                else
                {
                    if(a[i][j] == a[i-1][j] || a[i][j] == a[i+1][j] || a[i][j] == a[i][j-1])
                    {
                        return 14;
                    }
                }
            }
            if(i >0 && i<i0-1 && j > 0 && j<j0-1)
            {
                if(a[i][j] == a[i-1][j] || a[i][j] == a[i][j+1] || a[i][j] == a[i][j-1] || a[i][j] == a[i+1][j])
                {
                    printf("i == %d\n",i);
                    printf("j == %d\n",j);
                    return 15;
                }
            }
        }
    }

    return 0;
}