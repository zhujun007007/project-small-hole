import QtQuick 2.15
import QtGraphicalEffects 1.15
Item {
    id: interfac
    Image {
        id: end
        width: root.width
        height: root.height
        source: "jieshu.png"
    }
    MouseArea {
        propagateComposedEvents: true
        x: 210
        y: 500
        width: 100
        height: 50
        onClicked: {
            stack.pop();
            mouse.accepted = false
        }
    }

    Text {
        id: txt
        color: "#aabbcc"
        font.pixelSize: 60
        anchors.bottom: end.bottom
        anchors.horizontalCenter: end.horizontalCenter
        anchors.bottomMargin: 80
        text: qsTr("重新开始")
        MouseArea {
            anchors.fill: parent
            onClicked: {
                stack.pop()
                stack.push(com1)
            }
        }
    }

    Glow {
        id: glow
        anchors.fill: txt
        radius: 10
        samples: 17
        color: "#aa1122"
        source: txt

        SequentialAnimation {
            running: true
            loops: Animation.Infinite
            NumberAnimation {
                target: glow
                property: "spread"
                to:0
                duration: 1000
            }
            NumberAnimation {
                target: glow
                property: "spread"
                to: 0.5
                duration: 1000
            }
        }
    }

}
