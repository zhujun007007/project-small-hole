import QtQuick 2.15

Rectangle {
    z: 1
    width: 50
    height: 50
    id: rect1
    x: (mouse.mouseX - width/2 < 0) ? 0 : ((mouse.mouseX - width/2 > root.width - width) ? root.width - width : (mouse.mouseX - width/2))
    y: (mouse.mouseY - height/2 < 0) ? 0 : ((mouse.mouseY - height/2 > root.height - height) ? root.height - height : (mouse.mouseY - height/2))
    border.color: "#000000"
    opacity: 0.5


    Image {
        id: chuizi
        anchors.fill: parent
        source: "chuizi.png"
            Component.onCompleted: {
                focus: true
                rect1.forceActiveFocus()
            }
        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            onClicked: {
                mouse.accepted = false
            }
        }
    }

}

