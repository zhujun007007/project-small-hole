#ifndef MYDATA_H
#define MYDATA_H

#include <QObject>

class MyData : public QObject
{
    Q_OBJECT
public:
    explicit MyData(QObject *parent = nullptr);
    Q_INVOKABLE int myrand();
    Q_INVOKABLE void myexit();
    static MyData * getInstance();
    int mm;
signals:

};

#endif // MYDATA_H
