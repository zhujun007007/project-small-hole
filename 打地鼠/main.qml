import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
Window {
    width: 520
    height: 800
    visible: true
    id: root
    title: qsTr("打地鼠")
    Component {
        id: com2
        EndInterface {
            id: endInterface
        }
    }
    Component {
        id: com1
        MajorInterface {
            id: majorInterface
        }
    }
    Component {
        id: com0
        StartInterface {
            id: startInterface
        }
    }

    StackView {
         id: stack
         initialItem: com0
         anchors.fill: parent
     }

}
