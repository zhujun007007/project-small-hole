import QtQuick 2.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.15
import Myobj 1.0
Item {
    id: startInterface
    width: root.width
    height: root.height

    Column {
       Image {
           id:beijing11
           height: root.height * 0.25
           width: root.width
           source: "beijing1.png"

       }
       Image {
           id:beijing22
           height: root.height * 0.5
           width: root.width
           source: "kaishi.png"
           BusyIndicator {
               width: 100
               height: 100
               anchors.bottom: parent.bottom
               anchors.horizontalCenter: parent.horizontalCenter
               anchors.bottomMargin: 20
           }
           MouseArea {
               anchors.fill: parent
               onClicked: {
                   stack.push(com1)
               }
           }
       }
       Image {
           id:beijing33
           height: root.height * 0.25
           width: root.width
           source: "beijing1.png"
       }
    }


    Text {
        z: 1
        id: txt
        color: "#aabbcc"
        font.pixelSize: 60
        anchors.bottom: startInterface.bottom
        anchors.horizontalCenter: startInterface.horizontalCenter
        anchors.bottomMargin: 80
        text: qsTr("退出游戏")
        MouseArea {
            anchors.fill: parent
            onClicked: {
                MyData.myexit()
            }
        }
    }

    Glow {
        id: glow
        anchors.fill: txt
        radius: 10
        samples: 17
        color: "#aa1122"
        source: txt

        SequentialAnimation {
            running: true
            loops: Animation.Infinite
            NumberAnimation {
                target: glow
                property: "spread"
                to:0
                duration: 1000
            }
            NumberAnimation {
                target: glow
                property: "spread"
                to: 0.5
                duration: 1000
            }
        }
    }
}
