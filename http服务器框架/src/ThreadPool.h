#pragma once
#include <queue>
#include <pthread.h>
#include "Task.h"

#define NUM 6

//线程池
class ThreadPool {
private:
    std::queue<Task> _task_queue; //任务队列
    int _num;                     //线程池中线程的个数
    pthread_mutex_t _mutex;       //互斥锁
    pthread_cond_t _cond;         //条件变量
    static ThreadPool* _inst;     //指向单例对象的static指针
    bool stop;             //是否关闭整个线程池
private:
    //构造函数私有
    //构造函数私有
    ThreadPool(int num = NUM)
    {
        this->_num = num;
        this->stop = true;
        //初始化互斥锁和条件变量
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_cond, nullptr);
    }
    //将拷贝构造函数和拷贝赋值函数私有或删除（防拷贝）
    ThreadPool(const ThreadPool&) = delete;
    ThreadPool* operator=(const ThreadPool&) = delete;
    //判断任务队列是否为空
    bool IsEmpty();
    //任务队列加锁
    void LockQueue();
    //任务队列解锁
    void UnLockQueue();
    //让线程在条件变量下进行等待
    void ThreadWait();
    //唤醒在条件变量下等待的一个线程
    void ThreadWakeUp();
    //唤醒在条件变量下等待的所有线程
    void ThreadWakeUpAll();

public:
    //获取单例对象
    static ThreadPool* GetInstance();

    //线程的执行例程
    static void* ThreadRoutine(void* arg);

    //初始化线程池
    bool InitThreadPool();

    //将任务放入任务队列
    void PushTask(const Task& task);

    //从任务队列中拿任务
    void PopTask(Task& task);

    ~ThreadPool();
};


