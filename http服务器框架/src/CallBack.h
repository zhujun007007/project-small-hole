#pragma once
#include "log.h"
#include "EndPoint.h"
#include <unistd.h>

class CallBack {
public:
    CallBack()
    {}
    void operator()(int sock, HandlerHttpRequest* handlerHttp)
    {
        HandlerRequest(sock, handlerHttp);
    }
    void HandlerRequest(int sock, HandlerHttpRequest* handlerHttp);
    ~CallBack()
    {}
};


