#pragma once
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <pthread.h>
#include <cstring>
#include "log.h"
#define BACKLOG 5


//TCP服务器
class TcpServer {
private:
    int _port;              //端口号
    int _listen_sock;       //监听套接字
    static TcpServer* _svr; //指向单例对象的static指针
private:
    //构造函数私有
    TcpServer(int port)
        :_port(port)
        , _listen_sock(-1) {
    
    }
    //将拷贝构造函数和拷贝赋值函数私有或删除（防拷贝）
    TcpServer(const TcpServer&) = delete;
    TcpServer* operator=(const TcpServer&) = delete;
public:
    //获取单例对象
    static TcpServer* GetInstance(int port);
    //初始化服务器
    void InitServer();
    //创建套接字
    void Socket();
    //绑定
    void Bind();
    //监听
    void Listen();
    //获取监听套接字
    int Sock();
    ~TcpServer();
};

