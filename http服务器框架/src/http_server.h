#pragma once
#define PORT 8081
#include "HandlerHttpRequest.h"


//HTTP服务器
class HttpServer {
private:
    int _port; //端口号
    HandlerHttpRequest* _handlerHttp; // http服务器对于的处理逻辑
public:
    HttpServer(int port, HandlerHttpRequest* handlerHttp)
        :_port(port),
        _handlerHttp(handlerHttp)
    {}
    //启动服务器
    void Loop();
    ~HttpServer()
    {}
};
