#include "http_server.h"
#include "log.h"
#include "server_socket.h"
#include "CallBack.h"
#include "ThreadPool.h"
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <cstring>


//启动服务器
void HttpServer::Loop()
{
    LOG(INFO, "loop begin");
    TcpServer* tsvr = TcpServer::GetInstance(_port); //获取TCP服务器单例对象
    int listen_sock = tsvr->Sock(); //获取监听套接字
    while (true) {
        struct sockaddr_in peer;
        memset(&peer, 0, sizeof(peer));
        socklen_t len = sizeof(peer);
        int sock = accept(listen_sock, (struct sockaddr*)&peer, &len); //获取新连接
        if (sock < 0) {
            continue; //获取失败，继续获取
        }

        //打印客户端相关信息
        std::string client_ip = inet_ntoa(peer.sin_addr);
        int client_port = ntohs(peer.sin_port);
        LOG(INFO, "get a new link: [" + client_ip + ":" + std::to_string(client_port) + "]");

        //构建任务并放入任务队列中
        Task task(sock, _handlerHttp);
        ThreadPool::GetInstance()->PushTask(task);
    }
}