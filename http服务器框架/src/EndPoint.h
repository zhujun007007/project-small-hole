#pragma once
#include "HttpRequest.h"
#include "HttpResponse.h"
#include "log.h"
#include "Util.h"
#include "HandlerHttpRequest.h"

#include <sstream>
#include <algorithm>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unordered_map>
#include <sys/sendfile.h>
#include <sys/wait.h>

#define WEB_ROOT "wwwroot"
#define HOME_PAGE "index.html"
//服务端EndPoint
class EndPoint {
public:
    int _sock;                   //通信的套接字
    HttpRequest _http_request;   //HTTP请求
    HttpResponse _http_response; //HTTP响应
    bool _stop;                  //是否停止本次处理

    bool RecvHttpRequestLine();    //读取请求行
    bool RecvHttpRequestHeader();  //读取请求报头和空行
    void ParseHttpRequestLine();   //解析请求行
    void ParseHttpRequestHeader(); //解析请求报头
    bool RecvHttpRequestBody();    //读取请求正文
    bool IsNeedRecvHttpRequestBody();//判断是否需要读取请求正文
    void BuildOkResponse(); //构建响应报头
    void HandlerError(std::string page);//构建请求处理出现错误
  
public:
    //处理请求
    HandlerHttpRequest* _handlerHttp;

    EndPoint(int sock, HandlerHttpRequest* handlerHttp)
        :_sock(sock),
        _stop(false),
        _handlerHttp(handlerHttp)
    {}
    //本次处理是否停止
    bool IsStop();
    //读取请求
    void RecvHttpRequest();
    //处理请求
    //void HandlerHttpRequest();
    //构建响应
    void BuildHttpResponse();
    //发送响应
    bool SendHttpResponse();
    //CGI处理
    //int ProcessCgi();
    //非CGI处理
    //int ProcessNonCgi();
    ~EndPoint()
    {}
};
