#include "CallBack.h"

void CallBack::HandlerRequest(int sock, HandlerHttpRequest* handlerHttp)
{
    LOG(INFO, "handler request begin");

    EndPoint* ep = new EndPoint(sock, handlerHttp);
    ep->RecvHttpRequest(); //读取请求
    if (!ep->IsStop()) {
        LOG(INFO, "Recv No Error, Begin Handler Request");
        ep->_handlerHttp->HandlerHttp(ep); //处理请求
        ep->BuildHttpResponse();  //构建响应
        ep->SendHttpResponse();   //发送响应
        if (ep->IsStop()) {
            LOG(WARNING, "Send Error, Stop Send Response");
        }
    }
    else {
        LOG(WARNING, "Recv Error, Stop Handler Request");
    }

    close(sock); //关闭与该客户端建立的套接字
    delete ep;

    LOG(INFO, "handler request end");
}