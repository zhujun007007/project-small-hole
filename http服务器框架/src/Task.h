#pragma once
#include "CallBack.h"
#include "HandlerHttpRequest.h"
//任务类
class Task {
private:
    int _sock;         //通信的套接字
    HandlerHttpRequest* _handlerHtt; //对于的http处理逻辑
    CallBack _handler; //回调函数
public:
    Task()
    {}
    Task(int sock, HandlerHttpRequest* handlerHttp)
        :_sock(sock),
        _handlerHtt(handlerHttp)
    {}
    //处理任务
    void ProcessOn()
    {
        _handler(_sock, _handlerHtt); //调用回调
    }
    ~Task()
    {}
};

