#include "EndPoint.h"

#define SEP ": "
//读取请求行
bool EndPoint::RecvHttpRequestLine()
{
    auto& line = _http_request._request_line;
    if (Util::ReadLine(_sock, line) > 0) {
        line.resize(line.size() - 1); //去掉读取上来的\n
    }
    else { //读取出错，则停止本次处理
        _stop = true;
    }
    return _stop;
}
//读取请求报头和空行
bool EndPoint::RecvHttpRequestHeader()
{
    std::string line;
    while (true) {
        line.clear(); //每次读取之前清空line
        if (Util::ReadLine(_sock, line) <= 0) { //读取出错，则停止本次处理
            _stop = true;
            break;
        }
        if (line == "\n") { //读取到了空行
            _http_request._blank = line;
            break;
        }
        //读取到一行请求报头
        line.resize(line.size() - 1); //去掉读取上来的\n
        _http_request._request_header.push_back(line);
    }
    return _stop;
}
//解析请求行
void EndPoint::ParseHttpRequestLine()
{
    auto& line = _http_request._request_line;

    //通过stringstream拆分请求行
    std::stringstream ss(line);
    ss >> _http_request._method >> _http_request._uri >> _http_request._version;

    //将请求方法统一转换为全大写
    auto& method = _http_request._method;
    std::transform(method.begin(), method.end(), method.begin(), toupper);
}
//解析请求报头
void EndPoint::ParseHttpRequestHeader()
{
    std::string key;
    std::string value;
    for (auto& iter : _http_request._request_header) {
        //将每行请求报头打散成kv键值对，插入到unordered_map中
        if (Util::CutString(iter, key, value, SEP)) {
            _http_request._header_kv.insert({ key, value });
        }
    }
}

//判断是否需要读取请求正文
bool EndPoint::IsNeedRecvHttpRequestBody()
{
    auto& method = _http_request._method;
    if (method == "POST") { //请求方法为POST则需要读取正文
        auto& header_kv = _http_request._header_kv;
        //通过Content-Length获取请求正文长度
        auto iter = header_kv.find("Content-Length");
        if (iter != header_kv.end()) {
            _http_request._content_length = atoi(iter->second.c_str());
            return true;
        }
    }
    return false;
}
//读取请求正文
bool EndPoint::RecvHttpRequestBody()
{
    if (IsNeedRecvHttpRequestBody()) { //先判断是否需要读取正文
        int content_length = _http_request._content_length;
        auto& body = _http_request._request_body;

        //读取请求正文
        char ch = 0;
        while (content_length) {
            ssize_t size = recv(_sock, &ch, 1, 0);
            if (size > 0) {
                body.push_back(ch);
                content_length--;
            }
            else { //读取出错或对端关闭，则停止本次处理
                _stop = true;
                break;
            }
        }
    }
    return _stop;
}

//读取请求
void EndPoint::RecvHttpRequest()
{
    if (!RecvHttpRequestLine() && !RecvHttpRequestHeader()) { //短路求值
        ParseHttpRequestLine();  //解析请求行
        ParseHttpRequestHeader();//解析请求报头
        RecvHttpRequestBody();   //读取请求正文
    }
}


//根据状态码获取状态码描述
static std::string CodeToDesc(int code)
{
    std::string desc;
    switch (code) {
    case 200:
        desc = "OK";
        break;
    case 400:
        desc = "Bad Request";
        break;
    case 404:
        desc = "Not Found";
        break;
    case 500:
        desc = "Internal Server Error";
        break;
    default:
        break;
    }
    return desc;
}

//根据后缀获取资源类型
static std::string SuffixToDesc(const std::string& suffix)
{
    static std::unordered_map<std::string, std::string> suffix_to_desc = {
        {".html", "text/html"},
        {".css", "text/css"},
        {".js", "application/x-javascript"},
        {".jpg", "application/x-jpg"},
        {".xml", "text/xml"}
    };
    auto iter = suffix_to_desc.find(suffix);
    if (iter != suffix_to_desc.end()) {
        return iter->second;
    }
    return "text/html"; //所给后缀未找到则默认该资源为html文件
}

//构建响应报头
void EndPoint::BuildOkResponse()
{

    std::string content_type = "Content-Type: ";
    content_type += SuffixToDesc(_http_response._suffix);
    content_type += LINE_END;
    _http_response._response_header.push_back(content_type);

    std::string content_length = "Content-Length: ";
    if (_http_request._cgi) { //以CGI方式请求
        content_length += std::to_string(_http_response._response_body.size());
    }
    else { //以非CGI方式请求
        content_length += std::to_string(_http_response._size);
    }
    content_length += LINE_END;
    _http_response._response_header.push_back(content_length);
}

//构建响应
void EndPoint::BuildHttpResponse()
{
    int code = _http_response._status_code;
    //构建状态行
    auto& status_line = _http_response._status_line;
    status_line += HTTP_VERSION;
    status_line += " ";
    status_line += std::to_string(code);
    status_line += " ";
    status_line += CodeToDesc(code);
    status_line += LINE_END;

    //构建响应报头
    std::string path = WEB_ROOT;
    path += "/";
    switch (code) {
    case OK:
        BuildOkResponse();
        break;
    case NOT_FOUND:
        path += PAGE_404;
        HandlerError(path);
        break;
    case BAD_REQUEST:
        path += PAGE_400;
        HandlerError(path);
        break;
    case INTERNAL_SERVER_ERROR:
        path += PAGE_500;
        HandlerError(path);
        break;
    default:
        break;
    }
}

//构建请求处理出现错误
void EndPoint::HandlerError(std::string page)//构建请求处理出现错误
{
    _http_request._cgi = false; //需要返回对应的错误页面（非CGI返回）

    //打开对应的错误页面文件，以供后续发送
    _http_response._fd = open(page.c_str(), O_RDONLY);
    if (_http_response._fd > 0) { //打开文件成功
        //构建响应报头
        struct stat st;
        stat(page.c_str(), &st); //获取错误页面文件的属性信息

        std::string content_type = "Content-Type: text/html";
        content_type += LINE_END;
        _http_response._response_header.push_back(content_type);

        std::string content_length = "Content-Length: ";
        content_length += std::to_string(st.st_size);
        content_length += LINE_END;
        _http_response._response_header.push_back(content_length);

        _http_response._size = st.st_size; //重新设置响应文件的大小
    }
}
//发送响应
bool EndPoint::SendHttpResponse()
{
    //发送状态行
    if (send(_sock, _http_response._status_line.c_str(), _http_response._status_line.size(), 0) <= 0) {
        _stop = true; //发送失败，设置_stop
    }
    //发送响应报头
    if (!_stop) {
        for (auto& iter : _http_response._response_header) {
            if (send(_sock, iter.c_str(), iter.size(), 0) <= 0) {
                _stop = true; //发送失败，设置_stop
                break;
            }
        }
    }
    //发送空行
    if (!_stop) {
        if (send(_sock, _http_response._blank.c_str(), _http_response._blank.size(), 0) <= 0) {
            _stop = true; //发送失败，设置_stop
        }
    }
    //发送响应正文
    if (_http_request._cgi) {
        if (!_stop) {
            auto& response_body = _http_response._response_body;
            const char* start = response_body.c_str();
            size_t size = 0;
            size_t total = 0;
            while (total < response_body.size() && (size = send(_sock, start + total, response_body.size() - total, 0)) > 0) {
                total += size;
            }
        }
    } else {
        if (!_stop) {
            if (sendfile(_sock, _http_response._fd, nullptr, _http_response._size) <= 0) {
                _stop = true; //发送失败，设置_stop
            }
        }
        //关闭请求的资源文件
        close(_http_response._fd);
    }
    return _stop;
}


//本次处理是否停止
bool EndPoint::IsStop()
{
    return _stop;
}


