#pragma once
#include <string>
//工具类
class Util {
public:
    //读取一行
    static int ReadLine(int sock, std::string& out);
    //切割字符串
    static bool CutString(std::string& target, std::string& sub1_out, std::string& sub2_out, std::string sep);
};

