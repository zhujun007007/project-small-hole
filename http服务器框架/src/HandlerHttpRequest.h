#pragma once

class HandlerHttpRequest {
public:
	virtual void HandlerHttp(void* endPoint = nullptr) = 0;
};