#include "ThreadPool.h"
#include "log.h"
//单例对象指针初始化为nullptr
ThreadPool* ThreadPool::_inst = nullptr;
//判断任务队列是否为空
bool ThreadPool::IsEmpty()
{
    return _task_queue.empty();
}

//任务队列加锁
void ThreadPool::LockQueue()
{
    pthread_mutex_lock(&_mutex);
}

//任务队列解锁
void ThreadPool::UnLockQueue()
{
    pthread_mutex_unlock(&_mutex);
}

//让线程在条件变量下进行等待
void ThreadPool::ThreadWait()
{
    pthread_cond_wait(&_cond, &_mutex);
}

//唤醒在条件变量下等待的一个线程
void ThreadPool::ThreadWakeUp()
{
    pthread_cond_signal(&_cond);
}
//唤醒在条件变量下等待的所有线程
void ThreadPool::ThreadWakeUpAll()
{
    pthread_cond_broadcast(&_cond);
}

ThreadPool::~ThreadPool()
{
    //释放互斥锁和条件变量
    ThreadWakeUpAll();
    pthread_mutex_destroy(&_mutex);
    pthread_cond_destroy(&_cond);
    this->stop = false;
}

//获取单例对象
ThreadPool* ThreadPool::GetInstance()
{
    static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER; //定义静态的互斥锁
    //双检查加锁
    if (_inst == nullptr) {
        pthread_mutex_lock(&mtx); //加锁
        if (_inst == nullptr) {
            //创建单例线程池对象并初始化
            _inst = new ThreadPool();
            _inst->InitThreadPool();
        }
        pthread_mutex_unlock(&mtx); //解锁
    }
    return _inst; //返回单例对象
}

//线程的执行例程
void* ThreadPool::ThreadRoutine(void* arg)
{
    pthread_detach(pthread_self()); //线程分离
    ThreadPool* tp = (ThreadPool*)arg;
    while (true) {
        tp->LockQueue(); //加锁
        while (tp->IsEmpty() && tp->stop) {
            //任务队列为空，线程进行wait
            tp->ThreadWait();
        }
        if (tp->stop == false) {
            break;
        }
        Task task;
        tp->PopTask(task); //获取任务
        tp->UnLockQueue(); //解锁

        task.ProcessOn(); //处理任务
    }
    return NULL;
}

//初始化线程池
bool ThreadPool::InitThreadPool()
{
    //创建线程池中的若干线程
    pthread_t tid;
    for (int i = 0; i < _num; i++) {
        if (pthread_create(&tid, nullptr, ThreadRoutine, this) != 0) {
            LOG(FATAL, "create thread pool error!");
            return false;
        }
    }
    LOG(INFO, "create thread pool success");
    return true;
}

//将任务放入任务队列
void ThreadPool::PushTask(const Task& task)
{
    LockQueue();    //加锁
    _task_queue.push(task); //将任务推入任务队列
    UnLockQueue();  //解锁
    ThreadWakeUp(); //唤醒一个线程进行任务处理
}

//从任务队列中拿任务
void ThreadPool::PopTask(Task& task)
{
    //获取任务
    task = _task_queue.front();
    _task_queue.pop();
}