
#include "http_server.h"
#include <unistd.h>
#include <memory>
#include <iostream>
#include "HandlerHttpRequest.h"
#include "EndPoint.h"

class MyHandlerHttp : public HandlerHttpRequest {
public:
    //处理请求
    virtual void HandlerHttp(void* myendPoint)
    {
        EndPoint* endPoint = (EndPoint*)myendPoint;
        auto& code = endPoint->_http_response._status_code;

        if (endPoint->_http_request._method != "GET" && endPoint->_http_request._method != "POST") { //非法请求
            LOG(WARNING, "method is not right");
            code = BAD_REQUEST; //设置对应的状态码，并直接返回
            return;
        }

        if (endPoint->_http_request._method == "GET") {
            size_t pos = endPoint->_http_request._uri.find('?');
            if (pos != std::string::npos) { //uri中携带参数
                //切割uri，得到客户端请求资源的路径和uri中携带的参数
                Util::CutString(endPoint->_http_request._uri, endPoint->_http_request._path, endPoint->_http_request._query_string, "?");
                endPoint->_http_request._cgi = true; //上传了参数，需要使用CGI模式
            }
            else { //uri中没有携带参数
                endPoint->_http_request._path = endPoint->_http_request._uri; //uri即是客户端请求资源的路径
            }
        }
        else if (endPoint->_http_request._method == "POST") {
            endPoint->_http_request._path = endPoint->_http_request._uri; //uri即是客户端请求资源的路径
            endPoint->_http_request._cgi = true; //上传了参数，需要使用CGI模式
        }
        else {
            //Do Nothing
        }

        //给请求资源路径拼接web根目录
        std::string path = endPoint->_http_request._path;
        endPoint->_http_request._path = WEB_ROOT;
        endPoint->_http_request._path += path;

        //请求资源路径以/结尾，说明请求的是一个目录
        if (endPoint->_http_request._path[endPoint->_http_request._path.size() - 1] == '/') {
            //拼接上该目录下的index.html
            endPoint->_http_request._path += HOME_PAGE;
        }

        //获取请求资源文件的属性信息
        struct stat st;
        if (stat(endPoint->_http_request._path.c_str(), &st) == 0) { //属性信息获取成功，说明该资源存在
            if (S_ISDIR(st.st_mode)) { //该资源是一个目录
                endPoint->_http_request._path += "/"; //需要拼接/，以/结尾的目录前面已经处理过了
                endPoint->_http_request._path += HOME_PAGE; //拼接上该目录下的index.html
                stat(endPoint->_http_request._path.c_str(), &st); //需要重新资源文件的属性信息
            }
            else if (st.st_mode & S_IXUSR || st.st_mode & S_IXGRP || st.st_mode & S_IXOTH) { //该资源是一个可执行程序
                endPoint->_http_request._cgi = true; //需要使用CGI模式
            }
            endPoint->_http_response._size = st.st_size; //设置请求资源文件的大小
        }
        else { //属性信息获取失败，可以认为该资源不存在
            LOG(WARNING, endPoint->_http_request._path + " NOT_FOUND");
            code = NOT_FOUND; //设置对应的状态码，并直接返回
            return;
        }

        //获取请求资源文件的后缀
        size_t pos = endPoint->_http_request._path.rfind('.');
        if (pos == std::string::npos) {
            endPoint->_http_response._suffix = ".html"; //默认设置
        }
        else {
            endPoint->_http_response._suffix = endPoint->_http_request._path.substr(pos);
        }

        //进行CGI或非CGI处理
        if (endPoint->_http_request._cgi == true) {
            code = ProcessCgi(endPoint); //以CGI的方式进行处理
        }
        else {
            code = ProcessNonCgi(endPoint); //简单的网页返回，返回静态网页
        }
    }


    //CGI处理
    int ProcessCgi(EndPoint* endPoint)
    {
        int code = OK; //要返回的状态码，默认设置为200

        auto& bin = endPoint->_http_request._path;      //需要执行的CGI程序
        auto& method = endPoint->_http_request._method; //请求方法

        //需要传递给CGI程序的参数
        auto& query_string = endPoint->_http_request._query_string; //GET
        auto& request_body = endPoint->_http_request._request_body; //POST

        int content_length = endPoint->_http_request._content_length;  //请求正文的长度
        auto& response_body = endPoint->_http_response._response_body; //CGI程序的处理结果放到响应正文当中

        //1、创建两个匿名管道（管道命名站在父进程角度）
        //创建从子进程到父进程的通信信道
        int input[2];
        if (pipe(input) < 0) { //管道创建失败，则返回对应的状态码
            LOG(ERROR, "pipe input error!");
            code = INTERNAL_SERVER_ERROR;
            return code;
        }
        //创建从父进程到子进程的通信信道
        int output[2];
        if (pipe(output) < 0) { //管道创建失败，则返回对应的状态码
            LOG(ERROR, "pipe output error!");
            code = INTERNAL_SERVER_ERROR;
            return code;
        }

        //2、创建子进程
        pid_t pid = fork();
        if (pid == 0) { //child
            //子进程关闭两个管道对应的读写端
            close(input[0]);
            close(output[1]);

            //将请求方法通过环境变量传参
            std::string method_env = "METHOD";
            setenv((char*)method_env.c_str(), (char*)method.c_str(), 1);
            LOG(INFO, "GET Method, Add Query_String env:" + method_env);

            if (method == "GET") { //将query_string通过环境变量传参
                std::string query_env = "QUERY_STRING";
                setenv((char*)query_env.c_str(), (char*)query_string.c_str(), 1);
                LOG(INFO, "GET Method, Add Query_String env:" + query_env);
            }
            else if (method == "POST") { //将正文长度通过环境变量传参
                std::string content_length_env = "CONTENT_LENGTH";
                setenv((char*)content_length_env.c_str(), (char*)std::to_string(content_length).c_str(), 1);
                LOG(INFO, "POST Method, Add Content_Length env");
            }
            else {
                //Do Nothing
            }

            //3、将子进程的标准输入输出进行重定向
            dup2(output[0], 0); //标准输入重定向到管道的输入
            dup2(input[1], 1);  //标准输出重定向到管道的输出

            //4、将子进程替换为对应的CGI程序
            execl(bin.c_str(), bin.c_str(), nullptr);
            exit(1); //替换失败
        }
        else if (pid < 0) { //创建子进程失败，则返回对应的错误码
            LOG(ERROR, "fork error!");
            code = INTERNAL_SERVER_ERROR;
            return code;
        }
        else { //father
            //父进程关闭两个管道对应的读写端
            close(input[1]);
            close(output[0]);

            if (method == "POST") { //将正文中的参数通过管道传递给CGI程序
                const char* start = request_body.c_str();
                int total = 0;
                int size = 0;
                while (total < content_length && (size = write(output[1], start + total, request_body.size() - total)) > 0) {
                    total += size;
                }
            }

            //读取CGI程序的处理结果
            char ch = 0;
            while (read(input[0], &ch, 1) > 0) {
                response_body.push_back(ch);
            } //不会一直读，当另一端关闭后会继续执行下面的代码

            //等待子进程（CGI程序）退出
            int status = 0;
            pid_t ret = waitpid(pid, &status, 0);
            if (ret == pid) {
                if (WIFEXITED(status)) { //正常退出
                    if (WEXITSTATUS(status) == 0) { //结果正确
                        LOG(INFO, "CGI program exits normally with correct results");
                        code = OK;
                    }
                    else {
                        LOG(INFO, "CGI program exits normally with incorrect results");
                        code = BAD_REQUEST;
                    }
                }
                else {
                    LOG(INFO, "CGI program exits abnormally");
                    code = INTERNAL_SERVER_ERROR;
                }
            }

            //关闭两个管道对应的文件描述符
            close(input[0]);
            close(output[1]);
        }
        return code; //返回状态码
    }
    //非CGI处理
    int ProcessNonCgi(EndPoint* endPoint)
    {
        //打开客户端请求的资源文件，以供后续发送
        endPoint->_http_response._fd = open(endPoint->_http_request._path.c_str(), O_RDONLY);
        if (endPoint->_http_response._fd >= 0) { //打开文件成功
            return OK;
        }
        return INTERNAL_SERVER_ERROR; //打开文件失败
    }
};

static void Usage(std::string proc)
{
    std::cout << "Usage:\n\t" << proc << " port" << std::endl;
}
int main(int argc, char* argv[])
{
    if (argc != 2) {
        Usage(argv[0]);
        exit(4);
    }
    int port = atoi(argv[1]); //端口号
    MyHandlerHttp* handlerHttp = new MyHandlerHttp;
    std::shared_ptr<HttpServer> svr(new HttpServer(port, handlerHttp)); //创建HTTP服务器对象
    svr->Loop(); //启动服务器
    return 0;
}
